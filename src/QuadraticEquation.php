<?php


namespace App;


class QuadraticEquation
{
    public function solve(float $a, float $b, float $c): array
    {
        $e = 10**-5;

        if (abs($a) < $e) {
            throw new \InvalidArgumentException("Argument #1 (\$a) must be greater than $e");
        }

        $d = $b**2 - 4 * $a * $c;

        if ($d > $e) {
            $x1 = (-$b + sqrt($d))/(2 * $a);
            $x2 = (-$b - sqrt($d))/(2 * $a);

            return [$x1, $x2];
        }

        if ($d < -$e) {
            return [];
        }

        return [ (-$b)/(2 * $a) ];
    }
}
