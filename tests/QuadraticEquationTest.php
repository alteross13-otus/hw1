<?php


namespace App\Tests;


use App\QuadraticEquation;
use PHPUnit\Framework\TestCase;

final class QuadraticEquationTest extends TestCase
{
    /**
     * @dataProvider successfulTestCases
     */
    public function testSuccessfulCases(float $a, float $b, float $c, array $result, string $message): void
    {
        $equation = new QuadraticEquation();
        $this->assertEquals($result, $equation->solve($a, $b, $c), $message);
    }

    public function successfulTestCases(): array
    {
        return [
            //a             b       c       result              message
            [ 1,            0,      1,      [],                 "D < 0"                         ],
            [ 1,            0,      -1,     [1, -1],            "D > 0"                         ],
            [ 1,            2,      1,      [-1],               "D == 0"                        ],
            [ 0.24999975,   1,      1,      [-2.000002000002],  "D nearly 0 and abs(D) < 10^-5" ],
            [ 10**-4,       2,      10000,  [-10000],           "A nearly 0 and abs(A) > 10^-5" ],
        ];
    }

    /**
     * @dataProvider errorTestCases
     */
    public function testErrorCases($a, $b, $c, $exception)
    {
        $this->expectException($exception);
        $equation = new QuadraticEquation();
        $equation->solve($a, $b, $c);
    }

    public function errorTestCases(): array
    {
        return [
            //a         b       c       exception
            [ 10**-6,   1,      1,      \InvalidArgumentException::class ],
            [ "str",    1,      1,      \TypeError::class                ],
            [ 1,        "str",  1,      \TypeError::class                ],
            [ 1,        1,      "str",  \TypeError::class                ],
        ];
    }
}
