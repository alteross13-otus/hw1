<?php


namespace App\Tests;


use App\SomeClass;
use PHPUnit\Framework\TestCase;

class SomeClassTest extends TestCase
{
    public function testSomeMethod()
    {
        // add some code here
        $this->assertTrue((new SomeClass())->someMethod());
        // and there
    }
}
